package com.example.google_sheet_crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoogleSheetCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoogleSheetCrudApplication.class, args);
    }

}
