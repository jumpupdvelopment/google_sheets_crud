package com.example.google_sheet_crud.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.example.google_sheet_crud.entity.User;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;
import com.google.api.services.sheets.v4.model.ClearValuesRequest;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    @Autowired
    private final GoogleAuthorize googleAuthorize;
    @Value("${sheet.application.name}")
    private String applicationName;
    @Value("${spreadsheet.id}")
    private String spreadsheetId;
    @Value("${sheet.start.range}")
    private String startRange;
    @Value("${sheet.all.range}")
    private String allRange;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String USER_ENTERED = "USER_ENTERED";
    private static final String INSERT_ROWS = "INSERT_ROWS";
    private static final String RAW = "RAW";

    private Sheets getSheets() {
        try {
            NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            return new Sheets.Builder(httpTransport, JSON_FACTORY, googleAuthorize.getCredentials(httpTransport))
                .setApplicationName(applicationName)
                .build();
        } catch (IOException | GeneralSecurityException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private List<List<Object>> getAllListUser() {
        try {
            List<String> range = Arrays.asList(allRange);
            BatchGetValuesResponse readResult = getSheets().spreadsheets()
                .values()
                .batchGet(spreadsheetId)
                .setRanges(range)
                .execute();
            return readResult.getValueRanges()
                .get(0)
                .getValues();
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public User get(String id) {
        List<List<Object>> list = getAllListUser();
        try {
            List<Object> currentUser = list.stream()
                .filter(s -> s.contains(id))
                .findAny()
                .get();
            return new User(currentUser.get(0)
                .toString(),
                    currentUser.get(1)
                        .toString(),
                    currentUser.get(2)
                        .toString(),
                    currentUser.get(3)
                        .toString());
        } catch (IndexOutOfBoundsException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public String add(User user) {
        UUID uuid = UUID.randomUUID();
        ValueRange appendBody = new ValueRange().setValues(Arrays
            .asList(Arrays.asList(uuid.toString(), user.getFirstName(), user.getMiddleName(), user.getLastName())));
        try {
            return getSheets().spreadsheets()
                .values()
                .append(spreadsheetId, startRange, appendBody)
                .setValueInputOption(USER_ENTERED)
                .setInsertDataOption(INSERT_ROWS)
                .setIncludeValuesInResponse(Boolean.TRUE)
                .execute()
                .toString();
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public String delete(String id) {
        List<List<Object>> list = getAllListUser();
        try {
            list = list.stream()
                .filter(s -> !s.contains(id))
                .collect(Collectors.toCollection(ArrayList::new));
            Sheets.Spreadsheets.Values.Clear clearRequest = getSheets().spreadsheets()
                .values()
                .clear(spreadsheetId, allRange, new ClearValuesRequest());
            clearRequest.execute();
            ValueRange body = new ValueRange().setValues(list);
            Sheets.Spreadsheets.Values.Update request = getSheets().spreadsheets()
                .values()
                .update(spreadsheetId, startRange, body);
            request.setValueInputOption(RAW);
            return request.execute()
                .toString();
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public String update(String id, User user) {
        List<List<Object>> list = getAllListUser();
        try {
            list = list.stream()
                .filter(s -> !s.contains(id))
                .collect(Collectors.toCollection(ArrayList::new));
            Sheets.Spreadsheets.Values.Clear clearRequest = getSheets().spreadsheets()
                .values()
                .clear(spreadsheetId, allRange, new ClearValuesRequest());
            ValueRange body = new ValueRange().setValues(getChangedList(list, user, id));
            clearRequest.execute();
            Sheets.Spreadsheets.Values.Update request = getSheets().spreadsheets()
                .values()
                .update(spreadsheetId, startRange, body);
            request.setValueInputOption(RAW);
            return request.execute()
                .toString();
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    private List<List<Object>> getChangedList(List<List<Object>> list, User user, String id) {
        if (user.getFirstName() == null || user.getMiddleName() == null || user.getLastName() == null) {
            throw new RuntimeException("Write down all the fields");
        } else {
            list.add(Arrays.asList(id, user.getFirstName(), user.getMiddleName(), user.getLastName()));
            return list;
        }
    }
}
